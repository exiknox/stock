﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Stock.ViewModel;

namespace StockTests.ViewModelTests
{
    class MainViewModelTests
    {
        [Test]
        public void VerifyDefaultState()
        {
            MainViewModel vm = new MainViewModel();
            Assert.That(vm.FirstUserControlVisible, Is.EqualTo(true));
            Assert.That(vm.SecondUserControlVisible, Is.EqualTo(false));
        }

        [Test]
        public void VerifyChangement()
        {
            MainViewModel vm = new MainViewModel();
            vm.ChangeVisibility.Execute("0");
            Assert.That(vm.FirstUserControlVisible, Is.EqualTo(true));
            Assert.That(vm.SecondUserControlVisible, Is.EqualTo(false));
            vm.ChangeVisibility.Execute("1");
            Assert.That(vm.FirstUserControlVisible, Is.EqualTo(false));
            Assert.That(vm.SecondUserControlVisible, Is.EqualTo(true));
        }
    }
}
