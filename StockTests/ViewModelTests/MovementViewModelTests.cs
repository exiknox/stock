﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Stock.Models;
using Stock.ViewModel;

namespace StockTests.ViewModelTests
{
    class MovementViewModelTests
    {
        [Test]
        public void VerifyDefaultState()
        {
            IStock stock = new StockManager();
            IMovementViewModel vm = new MovementViewModel(stock);
            Assert.That(vm.Products.Count, Is.EqualTo(0));
            Assert.That(vm.Selected, Is.EqualTo(null));
        }
    }
}
