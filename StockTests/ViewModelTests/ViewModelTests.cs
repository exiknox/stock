﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Stock.Models;
using Stock.ViewModel;

namespace StockTests.ViewModelTests
{
    class ViewModelTests
    {
        [Test]
        public void VerifyDefaultState()
        {
            IStock stock = new StockManager();
            IEditProductViewModel vm = new EditProductViewModel(stock);
            Assert.That(vm.Products.Count, Is.EqualTo(0));
        }

        [Test]
        public void VerifyAddCommand()
        {
            IStock stock = new StockManager();
            IEditProductViewModel vm = new EditProductViewModel(stock);
            vm.Add.Execute(null);
            Assert.That(vm.Products.Count, Is.EqualTo(0));
            vm.Name = "";
            vm.Add.Execute(null);
            Assert.That(vm.Products.Count, Is.EqualTo(0));
            vm.Name = "produit";
            vm.Add.Execute(null);
            Assert.That(vm.Products.Count, Is.EqualTo(1));
            vm.UnitPrice = -1;
            vm.Add.Execute(null);
            Assert.That(vm.Products.Count, Is.EqualTo(1));
            vm.UnitPrice = 2;
            vm.Add.Execute(null);
            Assert.That(vm.Products.Count, Is.EqualTo(2));
        }
    }
}
