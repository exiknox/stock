﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Stock.Models;

namespace StockTests.ModelsTests
{
    class MovementTests
    {
        [Test]
        public void VerifyDefauktState()
        {
            DateTime now = DateTime.Now;
            IMovement movement = new Movement(now, 10, "Ajout en stock", true);
            Assert.That(movement.Date, Is.EqualTo(now));
            Assert.That(movement.Quantity, Is.EqualTo(10));
            Assert.That(movement.Label, Is.EqualTo("Ajout en stock"));
            Assert.That(movement.Input, Is.EqualTo(true));
        }
    }
}
