﻿using NUnit.Framework;
using Stock.Models;

namespace StockTests.ModelsTests
{
    class EntryTests
    {
        [Test]
        public void VerifyDefaultState()
        {
            IProduct product = new Product("Produit", 25);
            IEntry entry = new Entry(product);
            Assert.That(entry.Product, Is.EqualTo(product));
            Assert.That(entry.Movements.Count, Is.EqualTo(0));
        }
    }
}
