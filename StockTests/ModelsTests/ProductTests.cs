﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Stock.Models;

namespace StockTests.ModelsTests
{
    class ProductTests
    {
        [Test]
        public void VerifyDefaultState()
        {
            IProduct product = new Product("Produit", 20);
            Assert.That(product.Name, Is.EqualTo("Produit"));
            Assert.That(product.UnitPrice, Is.EqualTo(20));
            Assert.That(product.Value, Is.EqualTo(0));
        }

        [Test]
        public void VerifyEqualsRedefinition()
        {
            IProduct p1 = new Product("Produit", 20);
            IProduct p2 = new Product("Produit", 30);
            IProduct p3 = new Product("Article", 20);
            IProduct p4 = new Product("Produit", 20);
            Assert.That(p1.Equals(p2), Is.EqualTo(false));
            Assert.That(p1.Equals(p3), Is.EqualTo(false));
            Assert.That(p1.Equals(p4), Is.EqualTo(true));
        }
    }
}
