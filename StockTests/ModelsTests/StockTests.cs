﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Stock.Models;

namespace StockTests.ModelsTests
{
    class StockTests
    {
        [Test]
        public void VerifyDefaultState()
        {
            IStock stock = new StockManager();
            Assert.That(stock.Products.Count, Is.EqualTo(0));
        }

        [Test]
        public void VerifyStateAfterAddNewEntry()
        {
            IStock stock = new StockManager();
            IProduct toAdd = new Product("produit", 20);
            stock.AddProduct(toAdd);
            stock.AddMovement(toAdd, new Movement(DateTime.Today, 20, "ajout", true));
            Assert.That(stock.GetLastMovement(toAdd), Is.EqualTo(DateTime.Today));
        }

        [Test]
        public void VerifyStateAfterRemoveEntry()
        {
            IStock stock = new StockManager();
            IProduct toAdd = new Product("produit", 20);
            stock.AddProduct(toAdd);
            stock.AddMovement(toAdd, new Movement(DateTime.Today, 20, "ajout", true));

            stock.RemoveProduct(toAdd);
            Assert.That(stock.Products.Count, Is.EqualTo(0));
            Assert.That(stock.GetLastMovement(toAdd), Is.EqualTo(DateTime.MinValue));
        }
    }
}
