﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Stock.Models;

namespace Stock.ViewModel
{
    public interface IMovementViewModel
    {
        RelayCommand Input { get; }
        RelayCommand Output { get; }
        DateTime MovementDateTime { get; set; }
        int Quantity { get; set; }
        string Label { get; set; }
        ObservableCollection<IProduct> Products { get; }
        IProduct Selected { get; set; }
    }

    public class MovementViewModel : ViewModelBase, IMovementViewModel, INotifyPropertyChanged
    {
        private readonly IStock _stock;
        public ObservableCollection<IProduct> Products => _stock.Products;
        public RelayCommand Input { get; }
        public RelayCommand Output { get; }
        private DateTime _movementDateTime;
        public DateTime MovementDateTime
        {
            get => _movementDateTime;
            set
            {
                _movementDateTime = value;
                CanExecute();
            }
        }

        private int _quantity;
        public int Quantity
        {
            get => _quantity;
            set
            {
                _quantity = value;
                CanExecute();
            } 
        }

        private string _label;
        public string Label
        {
            get => _label;
            set
            {
                _label = value;
                CanExecute();
            }
        }
        private IProduct _selected;
        public IProduct Selected
        {
            get => _selected;
            set
            {
                _selected = value;
                CanExecute();
            }
        }

        public MovementViewModel(IStock stock)
        {
            _stock = stock;
            _movementDateTime = DateTime.Now;
            Input = new RelayCommand(
                () =>
                {
                    if (Selected != null)
                    {
                        _stock.AddMovement(Selected, new Movement(_movementDateTime, _quantity, _label,true));
                        CanExecute();
                        OnPropertyChanged("Products");
                    }
                },
                () =>
                {
                    if (Quantity <= 0 || string.IsNullOrEmpty(Label) || Selected == null)
                        return false;
                    if(Selected != null && _stock.GetLastMovement(Selected)!= DateTime.MinValue)
                        if (_movementDateTime < _stock.GetLastMovement(Selected))
                            return false;
                    return true;
                }
            );
            Output = new RelayCommand(
                () =>
                {
                    if (Selected != null)
                    {
                        _stock.AddMovement(Selected, new Movement(_movementDateTime, _quantity, _label, false));
                        CanExecute();
                        OnPropertyChanged("Products");
                    }
                },
                () =>
                {
                    if (Quantity <= 0 || string.IsNullOrEmpty(Label) || Selected == null)
                        return false;
                    if (Selected.Value < Quantity)
                        return false;
                    if(_stock.GetLastMovement(Selected) != DateTime.MinValue)
                        if (_movementDateTime < _stock.GetLastMovement(Selected))
                            return false;
                    return true;
                }
            );
        }

        private void CanExecute()
        {
            Input.RaiseCanExecuteChanged();
            Output.RaiseCanExecuteChanged();
        }

        public new event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
