﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Stock.Models;

namespace Stock.ViewModel
{
    public interface IEditProductViewModel
    {
        RelayCommand Add { get; }
        string Name { get; set; }
        double UnitPrice { get; set; }
        ObservableCollection<IProduct> Products { get; }
    }

    public class EditProductViewModel : ViewModelBase, IEditProductViewModel, INotifyPropertyChanged
    {
        private readonly IStock _stock;
        public ObservableCollection<IProduct> Products => _stock.Products;
        public RelayCommand Add { get; }

        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                Add.RaiseCanExecuteChanged();
            }
        }

        private double _unitPrice;
        public double UnitPrice
        {
            get => _unitPrice;
            set
            {
                _unitPrice = value;
                Add.RaiseCanExecuteChanged();
            }
        }

        public EditProductViewModel(IStock stock)
        {
            _stock = stock;
            Add = new RelayCommand(
                () =>
                {
                    IProduct product = new Product(Name, UnitPrice);
                    ((Product) product).ItemRemoved += this.OnItemRemoved;
                    _stock.AddProduct(product);
                    OnPropertyChanged("Products");
                    Add.RaiseCanExecuteChanged();
                },
                ()=>
                {
                    if (string.IsNullOrEmpty(Name) || UnitPrice < 0)
                        return false;
                    foreach (IProduct current in Products)
                        if (current.Name.Equals(Name) && Math.Abs(current.UnitPrice - UnitPrice) < 0.01)
                            return false;
                    return true;
                });
        }

        private void OnItemRemoved(object parameter, EventArgs args)
        {
            if (parameter is IProduct product)
                _stock.RemoveProduct(product);
            OnPropertyChanged("Products");
            Add.RaiseCanExecuteChanged();
        }

        public new event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
