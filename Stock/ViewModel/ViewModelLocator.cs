using System.Collections.ObjectModel;
using System.Security.Cryptography.X509Certificates;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Stock.Models;

namespace Stock.ViewModel
{
    public class ViewModelLocator
    {
        private IStock _stock;
        public MainViewModel Main => new MainViewModel();
        public IEditProductViewModel Edit => new EditProductViewModel(_stock);
        public IMovementViewModel Movement => new MovementViewModel(_stock);

        public ViewModelLocator()
        {
            _stock = new StockManager();
        }
    }
}