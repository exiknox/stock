using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Stock.Models;

namespace Stock.ViewModel
{
    public class MainViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private bool _firstUserControlVisible;
        public bool FirstUserControlVisible
        {
            get => _firstUserControlVisible;
            set
            {
                _firstUserControlVisible = value;
                _secondUserControlVisible = !value;
                OnPropertyChanged("FirstUserControlVisible");
                OnPropertyChanged("SecondUserControlVisible");
            }
        }

        private bool _secondUserControlVisible;
        public bool SecondUserControlVisible
        {
            get => _secondUserControlVisible;
            set
            {
                _secondUserControlVisible = value;
                _firstUserControlVisible = !value;
                OnPropertyChanged("FirstUserControlVisible");
                OnPropertyChanged("SecondUserControlVisible");
            }
        }

        public RelayCommand<object> ChangeVisibility { get; }
        public new event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public MainViewModel()
        {
            FirstUserControlVisible = true;
            SecondUserControlVisible = !FirstUserControlVisible;
            ChangeVisibility = new RelayCommand<object>(
                (e) =>
                {
                    if (e.Equals("0"))
                        FirstUserControlVisible = true;
                    else if (e.Equals("1"))
                        SecondUserControlVisible = true;
                }    
            );
        }
    }
}