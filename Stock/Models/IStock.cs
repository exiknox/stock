﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Stock.Models
{
    public interface IStock
    {
        ObservableCollection<IProduct> Products { get; }
        void RemoveProduct(IProduct p);
        void AddProduct(IProduct product);
        void AddMovement(IProduct product, IMovement movement);
        DateTime GetLastMovement(IProduct selected);
    }

    public class StockManager : IStock
    {
        private ObservableCollection<IEntry> Entries { get; }
        private ObservableCollection<IProduct> _products;
        public ObservableCollection<IProduct> Products
        {
            get
            {
                if(_products == null)
                    _products = new ObservableCollection<IProduct>();
                foreach (IEntry current in Entries)
                    if(!_products.Contains(current.Product))
                        _products.Add(current.Product);
                return _products;
            }
        }

        public void AddMovement(IProduct product, IMovement movement)
        {
            if(movement.Input)
                product.Value += movement.Quantity;
            else if (!movement.Input)
                product.Value -= movement.Quantity;
            foreach (IEntry t in Entries)
                if(t.Product.Equals(product))
                    t.Movements.Add(movement);
        }

        public DateTime GetLastMovement(IProduct selected)
        {
            foreach (IEntry current in Entries)
                if (current.Product.Equals(selected))
                    if(current.Movements.Count>0)
                        return current.Movements.Last().Date;
            return DateTime.MinValue;
        }

        public void RemoveProduct(IProduct p)
        {
            for (int i = 0; i < Entries.Count; i++)
                if (Entries[i].Product.Equals(p))
                    Entries.Remove(Entries[i]);
            if(_products != null)
                _products.Remove(p);
        }

        public void AddProduct(IProduct product)
        {
            Entries.Add(new Entry(product));
        }

        public StockManager()
        {
            Entries = new ObservableCollection<IEntry>();
        }
    }
}
