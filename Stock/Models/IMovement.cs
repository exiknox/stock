﻿using System;

namespace Stock.Models
{
    public interface IMovement
    {
        DateTime Date { get; }
        int Quantity { get; }
        string Label { get; }
        bool Input { get; }
    }

    public class Movement : IMovement
    {
        public DateTime Date { get; }
        public int Quantity { get; }
        public string Label { get; }
        public bool Input { get; }

        public Movement(DateTime date, int quantity, string label, bool input)
        {
            Date = date;
            Quantity = quantity;
            Label = label;
            Input = input;
        }
    }
}
