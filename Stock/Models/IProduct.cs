﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using GalaSoft.MvvmLight.Command;

namespace Stock.Models
{
    public interface IProduct
    {
        string Name { get; }
        double UnitPrice { get; }
        int Value { get; set; }
        RelayCommand Remove { get; }
    }

    public class Product : IProduct, INotifyPropertyChanged
    {
        public string Name { get; }
        public double UnitPrice { get; }
        private int _value;

        public int Value
        {
            get => _value;
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

        public RelayCommand Remove { get; }

        public Product(string name, double unitPrice) : this(name, unitPrice, 0)
        {
        }

        public Product(string name, double unitPrice, int value)
        {
            Name = name;
            UnitPrice = unitPrice;
            Value = value;
            Remove = new RelayCommand(OnItemRemoved);
        }

        public delegate void ItemRemovedEventHandler(object parameter, EventArgs args);

        public event ItemRemovedEventHandler ItemRemoved;

        private void OnItemRemoved()
        {
            ItemRemoved?.Invoke(this, EventArgs.Empty);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override bool Equals(object obj)
        {
            if (obj is IProduct product)
                return Name.Equals(product.Name) && Math.Abs(UnitPrice - product.UnitPrice) < 0.01;
            return false;
        }
    }
}
