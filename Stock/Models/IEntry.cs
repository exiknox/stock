﻿using System.Collections.ObjectModel;

namespace Stock.Models
{
    public interface IEntry
    {
        IProduct Product { get; set; }
        ObservableCollection<IMovement> Movements { get; }
    }

    public class Entry : IEntry
    {
        public IProduct Product { get; set; }
        public ObservableCollection<IMovement> Movements { get; }

        public Entry(IProduct product)
        {
            Product = product;
            Movements = new ObservableCollection<IMovement>();
        }
    }
}
